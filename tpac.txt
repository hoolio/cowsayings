## WTF IS THIS?
# anything with a # will be ignored, anything without a # will be said.  nuff said?
#
## ABOUT TPAC ##
What's TPAC anyhow? \n\nThe Tasmanian Partnership of Advanced Computing (TPAC) is located at the University of Tasmania in Hobart (Australia).
#
## GETTING HELP
Confused?  Need help?  Need a mentor? \n\nEmail helpdesk@tpac.org.au and we'll do our best to help!
Getting slowdowns? Something weird happening? \n\nSee the status of the various TPAC services at http://status.tpac.org.au/
#
## RANGE OF COMPUTING OFFERINGS
Want simple access to a computer within your browser? \n\nTry the TPAC Marine Cloud at https://mcportal.tpac.org.au/ - it's as easy as click click and go.
Do you want total control of your cloud computer? \n\nHave a look at the Nectar Research Cloud, get a generous compute and storage allocation to make a single instance, or 100 (or more!).
Need ultra performance?! \n\nRead up on our High Performance Computing (HPC) environments (we have several) at http://www.tpac.org.au/hpc-faq/
#
## STORAGE
Have so much data you don't know what to do? \n\nTPAC can store simply massive data sets, just ask us for help!
Tired of carrying USB keys?  Need to transfer 10TB from Japan? \n\nTPAC can help!  email helpdesk@tpac.org.au with your transfer and storage woes, and we'll make you smile again!
Confused about getting data to and from this environment? \n\nUse AARNET cloudstor to synchronize data right here from your laptop, automatically!
#
## ANACONDA NAVIGATOR
Want rstudio 1.0.153 not 1.0.44? Or how about both at the same time?  Want gluevis, spyder, orange3 and more?? Sure you do!\n\nExplore the possibilities using Anaconda Navigator.
Want a cheatsheet for Conda?\n\nSolved.  Have a look in your ~/Documents :)
What are Conda and Anaconda Navigator?\n\nConda is an open source package and environment management system (like pip and apt) for installing multiple versions of software packages and their dependencies and switching easily between them.\n\nAnaconda Navigator is a graphical tool which allows you easily manage Conda.  For example, installing rstudio in Anaconda Navigator takes a single click.  Yep.
Why does my bash prompt have (root) at the start??\n\nFirstly, for that to have happened, you must have installed Conda successfully, congratulations!  What this actually shows is that the "root" Conda environment is active and it's ready for you to install software!  The root environment is just the name of the default one.
#
## HELP US HELP YOU!
Can't find the software you need? \n\nThis virtual computer has been made with the cooperation of users just like you.  If you want something that's not here then please let us know by emailing helpdesk@tpac.org.au.
#
## FLUFF
moo.
moooooooooo.
Did you know that cashews come from a fruit?
#
## SHUTUP
Getting sick of these messages?? \n\nJust delete ~/.cancowsay :)
